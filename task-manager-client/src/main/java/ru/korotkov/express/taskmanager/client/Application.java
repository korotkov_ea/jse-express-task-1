package ru.korotkov.express.taskmanager.client;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.korotkov.express.taskmanager.client.executor.CommandExecutorImpl;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponse;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponseCode;
import ru.korotkov.express.taskmanager.client.utils.UserSession;

public class Application {

	private static final Logger logger = LogManager.getLogger(Application.class);

	private static final CommandExecutorImpl commandExecutorImpl = new CommandExecutorImpl();

	private static final UserSession userSession = UserSession.getInstance();

	public static void main(String[] args) {
		
		try (final Scanner scanner = new Scanner(System.in)) {
			var commandExecutorResponse = new CommandExecutorResponse(
					CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			while (!commandExecutorResponse.getCode().equals(CommandExecutorResponseCode.COMMAND_EXIT)) {
				var userLogin = userSession.getLogin();
				if (userLogin != null && !userLogin.isEmpty())
					logger.info("TaskManager@{}>Enter Command: ", userLogin);
				else
					logger.info("TaskManager>Enter Command: ");
				var command = scanner.nextLine();
				logger.trace("Entered command: \"{}\"", command);
				commandExecutorResponse = commandExecutorImpl.executeCommand(command, userSession.getLogin());
				logger.trace("Result of execution command: {}", commandExecutorResponse);
			}
		}
	}

}
