package ru.korotkov.express.taskmanager.client.command;

import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public abstract class Command {

	protected final Options options = new Options();

	public Options getOptions() {
		return options;
	}

	protected boolean isCommand(CommandLine commandLine) {
		if (commandLine != null) {
			for (Option option : options.getOptions()) {
				if (commandLine.hasOption(option.getOpt()))
					return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		ArrayList<String> commandsInfo = new ArrayList<>();
		options.getOptions().forEach(option -> {
			String string = "";
			string += "\n-" + option.getOpt();
			if (option.hasLongOpt())
				string += " --" + option.getLongOpt();
			string += " - " + option.getDescription();
			commandsInfo.add(string);
		});
		return getClass().getSimpleName() + ":" + commandsInfo;
	}

}
