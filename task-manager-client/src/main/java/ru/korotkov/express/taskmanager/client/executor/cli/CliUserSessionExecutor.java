package ru.korotkov.express.taskmanager.client.executor.cli;

import java.net.URI;
import java.util.Collection;
import java.util.Objects;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import ru.korotkov.express.taskmanager.client.command.CommandParam;
import ru.korotkov.express.taskmanager.client.command.UserSessionCommand;
import ru.korotkov.express.taskmanager.client.config.AppConfig;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponse;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponseCode;
import ru.korotkov.express.taskmanager.client.utils.Hash;
import ru.korotkov.express.taskmanager.client.utils.UserSession;
import ru.korotkov.express.taskmanager.server.rest.model.User;

public class CliUserSessionExecutor implements CliCommandExecutor {

	private final UserSessionCommand userSessionCommand = new UserSessionCommand();

	private final Logger logger = LogManager.getLogger(this);

	private final ApplicationContext context;
	private final RestOperations restOperations;

	public CliUserSessionExecutor() {
		context = new AnnotationConfigApplicationContext(AppConfig.class);
		restOperations = context.getBean(RestOperations.class);
	}

	@Override
	public CommandExecutorResponse executeCommand(CommandLine commandLine, String login) {
		if (commandLine.hasOption(UserSessionCommand.USER_SESSION_COMMAND_LOGIN))
			return executeUserSessionCommandLogin(commandLine);
		if (commandLine.hasOption(UserSessionCommand.USER_SESSION_COMMAND_LOGOUT))
			return executeUserSessionCommandLogout();
		return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
	}

	@Override
	public boolean isCommandForThisExecutor(CommandLine commandLine) {
		return userSessionCommand.isUserSessionCommand(commandLine);
	}

	@Override
	public Options getOptions() {
		return userSessionCommand.getOptions();
	}

	@Override
	public Collection<Option> getOptionsCollection() {
		return userSessionCommand.getOptions().getOptions();
	}

	CommandExecutorResponse executeUserSessionCommandLogin(CommandLine commandLine) {
		var login = commandLine.getOptionValue(CommandParam.COMMAND_PARAM_USER_LOGIN_SHORT);
		var password = commandLine.getOptionValue(CommandParam.COMMAND_PARAM_USER_PASSWORD_SHORT);
		var encryptedPassword = Hash.hashPassword(password);

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setBasicAuth("task-manager", "tm");
			HttpEntity<Void> request = new HttpEntity<Void>(headers);
			ResponseEntity<User> response = restOperations.exchange(URI.create("http://localhost:8080/users/" + login),
					HttpMethod.GET, request, User.class);
			if (response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
				logger.info("User with login \"{}\" not found", login);
				new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			}
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				var user = response.getBody();
				if (!Objects.equals(encryptedPassword, user.getPassword())) {
					logger.info("Access denied. Entered wrong login or password");
					return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
				}
				var userSession = UserSession.getInstance();
				userSession.setLogin(user.getLogin());
				logger.info("Welcome, {}!", user.getLogin());
				return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			}
			logger.info("Login failed. Http-code: {}", response.getStatusCode());
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		} catch (RestClientException e) {
			logger.error("Error while getting user: ", e);
			logger.info("Login failed with http error");
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		}
	}

	CommandExecutorResponse executeUserSessionCommandLogout() {
		var userSession = UserSession.getInstance();
		userSession.setLogin(null);
		return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_EXIT);
	}

}
