package ru.korotkov.express.taskmanager.client.utils;

/**
 * User session class. It gives access to information about current user in this
 * session
 * 
 * @author RemanentCrane
 */
public class UserSession {

	private UserSession() {

	}

	/** Login */
	private String login;

	/** Instance of this session */
	private static UserSession instance = null;

	/**
	 * Get instance of {@link UserSession} to access for information about current session
	 * @return {@link UserSession#instance} 
	 */
	public static UserSession getInstance() {
		if (instance == null)
			instance = new UserSession();
		return instance;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
