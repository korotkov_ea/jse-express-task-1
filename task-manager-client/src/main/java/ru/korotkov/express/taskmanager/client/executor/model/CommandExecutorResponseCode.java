package ru.korotkov.express.taskmanager.client.executor.model;

/**
 * Enum for determining the response for the
 * {@link ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponse}.
 * 
 * @author RemanentCrane
 */
public enum CommandExecutorResponseCode {

	/** Command successfully done */
	COMMAND_SUCCESSFULLY_COMPLETED(0),
	/** Command executed with error */
	COMMAND_COMPLETED_WITH_ERROR(1),
	/** Command was sent to shutdown task-manager */
	COMMAND_EXIT(-1);

	private final int code;

	private CommandExecutorResponseCode(final Integer code) {
		this.code = code;
	}

	public short getCode() {
		return (short) code;
	}

	@Override
	public String toString() {
		return "Listener response: " + code;
	}

}
