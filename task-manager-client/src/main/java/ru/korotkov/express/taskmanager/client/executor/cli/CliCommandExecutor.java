package ru.korotkov.express.taskmanager.client.executor.cli;

import java.util.Collection;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponse;

public interface CliCommandExecutor {

	public CommandExecutorResponse executeCommand(CommandLine commandLine, String login);

	public boolean isCommandForThisExecutor(CommandLine commandLine);

	public Options getOptions();

	public Collection<Option> getOptionsCollection();

}
