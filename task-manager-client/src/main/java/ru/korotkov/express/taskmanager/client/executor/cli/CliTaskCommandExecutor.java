package ru.korotkov.express.taskmanager.client.executor.cli;

import java.net.URI;
import java.util.Collection;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import ru.korotkov.express.taskmanager.client.command.CommandParam;
import ru.korotkov.express.taskmanager.client.command.TaskCommand;
import ru.korotkov.express.taskmanager.client.config.AppConfig;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponse;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponseCode;
import ru.korotkov.express.taskmanager.server.rest.model.AppResponse;
import ru.korotkov.express.taskmanager.server.rest.model.Task;
import ru.korotkov.express.taskmanager.server.rest.model.TaskBody;

public class CliTaskCommandExecutor implements CliCommandExecutor {

	private final TaskCommand taskCommand = new TaskCommand();

	private final Logger logger = LogManager.getLogger(this);

	private final ApplicationContext context;
	private final RestOperations restOperations;

	public CliTaskCommandExecutor() {
		context = new AnnotationConfigApplicationContext(AppConfig.class);
		restOperations = context.getBean(RestOperations.class);
	}

	@Override
	public CommandExecutorResponse executeCommand(CommandLine commandLine, String login) {
		if (commandLine.hasOption(TaskCommand.TASK_COMMAND_CREATE_SHORT))
			return executeCreateTask(commandLine, login);
		if (commandLine.hasOption(TaskCommand.TASK_COMMAND_LIST_ASSIGNED_SHORT))
			return executeTaskAssignedList(login);
		if (commandLine.hasOption(TaskCommand.TASK_COMMAND_LIST_CREATED_SHORT))
			return executeTaskCreatedList(login);
		if (commandLine.hasOption(TaskCommand.TASK_COMMAND_DELETE_SHORT))
			return executeDeleteTask(commandLine, login);
		if (commandLine.hasOption(TaskCommand.TASK_COMMAND_COMPLETE_SHORT))
			return executeCompleteTask(commandLine, login);
		return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
	}

	@Override
	public boolean isCommandForThisExecutor(CommandLine commandLine) {
		return taskCommand.isTaskCommand(commandLine);
	}

	@Override
	public Options getOptions() {
		return taskCommand.getOptions();
	}

	@Override
	public Collection<Option> getOptionsCollection() {
		return taskCommand.getOptions().getOptions();
	}

	CommandExecutorResponse executeCreateTask(CommandLine commandLine, String login) {
		var taskName = commandLine.getOptionValue(CommandParam.COMMAND_PARAM_NAME);
		var taskAssigned = commandLine.getOptionValue(CommandParam.COMMAND_PARAM_TASK_ASSIGNED);
		if (taskAssigned == null || taskAssigned.trim().isEmpty())
			taskAssigned = login;
		var task = new TaskBody().name(taskName).assigned(taskAssigned).created(login);

		try {
			var headers = new HttpHeaders();
			headers.setBasicAuth("task-manager", "tm");
			headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			HttpEntity<TaskBody> request = new HttpEntity<>(task, headers);
			ResponseEntity<AppResponse> response = restOperations.exchange(URI.create("http://localhost:8080/tasks"),
					HttpMethod.POST, request, AppResponse.class);
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				logger.info(response.getBody().getMessage());
				return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			}
			logger.info("Error while executing command \"complete task\": http-code:{}", response.getStatusCode());
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		} catch (RestClientException e) {
			logger.error("Error while creating task: ", e);
			logger.info("Failed when trying to creating task");
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		}
	}

	CommandExecutorResponse executeTaskAssignedList(String login) {
		try {
			var headers = new HttpHeaders();
			headers.setBasicAuth("task-manager", "tm");
			HttpEntity<Void> request = new HttpEntity<>(headers);
			ResponseEntity<List<Task>> response = restOperations.exchange(
					URI.create("http://localhost:8080/tasks/" + login + "/assigned"), HttpMethod.GET, request,
					new ParameterizedTypeReference<List<Task>>() {
					});
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				logger.info("Assigned on you tasks: {}", response.getBody());
				return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			}
			logger.info("Error while executing command \"get assigned task list\": http-code:{}",
					response.getStatusCode());
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		} catch (RestClientException e) {
			logger.error("Error while getting assigned task list: ", e);
			logger.info("Failed when getting assigned task list");
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		}
	}

	CommandExecutorResponse executeTaskCreatedList(String login) {
		try {
			var headers = new HttpHeaders();
			headers.setBasicAuth("task-manager", "tm");
			HttpEntity<Void> request = new HttpEntity<>(headers);
			ResponseEntity<List<Task>> response = restOperations.exchange(
					URI.create("http://localhost:8080/tasks/" + login + "/created"), HttpMethod.GET, request,
					new ParameterizedTypeReference<List<Task>>() {
					});
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				logger.info("Created by you tasks: {}", response.getBody());
				return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			}
			logger.info("Error while executing command \"get created task list\": http-code:{}",
					response.getStatusCode());
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		} catch (RestClientException e) {
			logger.error("Error while getting created task list: ", e);
			logger.info("Failed when getting created task list");
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		}
	}

	CommandExecutorResponse executeDeleteTask(CommandLine commandLine, String login) {
		var taskId = commandLine.getOptionValue(CommandParam.COMMAND_PARAM_TASK_ID);
		try {
			var headers = new HttpHeaders();
			headers.setBasicAuth("task-manager", "tm");
			headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			HttpEntity<Void> request = new HttpEntity<>(headers);
			ResponseEntity<AppResponse> response = restOperations.exchange(
					URI.create("http://localhost:8080/tasks/" + taskId + "?login=" + login), HttpMethod.POST, request,
					AppResponse.class);
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				logger.info(response.getBody().getMessage());
				return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			}
			logger.info("Error while executing command \"deleting task\": http-code:{}", response.getStatusCode());
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		} catch (RestClientException e) {
			logger.error("Error while deleting task: ", e);
			logger.info("Failed when trying to deleting task");
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		}
	}

	CommandExecutorResponse executeCompleteTask(CommandLine commandLine, String login) {
		var taskId = commandLine.getOptionValue(CommandParam.COMMAND_PARAM_TASK_ID_SHORT);
		try {
			var headers = new HttpHeaders();
			headers.setBasicAuth("task-manager", "tm");
			headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			HttpEntity<Void> request = new HttpEntity<>(headers);
			ResponseEntity<AppResponse> response = restOperations.exchange(
					URI.create("http://localhost:8080/tasks/" + taskId + "?login=" + login), HttpMethod.POST, request,
					AppResponse.class);
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				logger.info(response.getBody().getMessage());
				return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_SUCCESSFULLY_COMPLETED);
			}
			logger.info("Error while executing command \"complete task\": http-code:{}", response.getStatusCode());
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		} catch (RestClientException e) {
			logger.error("Error while completing task: ", e);
			logger.info("Failed when trying to complete task");
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		}
	}

}
