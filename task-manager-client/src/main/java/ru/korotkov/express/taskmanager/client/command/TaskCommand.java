package ru.korotkov.express.taskmanager.client.command;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;

public class TaskCommand extends Command {

	public static final String TASK_COMMAND_CREATE = "task_create";
	public static final String TASK_COMMAND_CREATE_SHORT = "tc";
	public static final String TASK_COMMAND_LIST_ASSIGNED = "task_list_assigned";
	public static final String TASK_COMMAND_LIST_ASSIGNED_SHORT = "tla";
	public static final String TASK_COMMAND_LIST_CREATED = "task_list_created";
	public static final String TASK_COMMAND_LIST_CREATED_SHORT = "tlc";
	public static final String TASK_COMMAND_DELETE = "task_delete";
	public static final String TASK_COMMAND_DELETE_SHORT = "td";
	public static final String TASK_COMMAND_COMPLETE = "task_complete";
	public static final String TASK_COMMAND_COMPLETE_SHORT = "tcom";

	public TaskCommand() {
		options.addOption(new Option(TASK_COMMAND_CREATE_SHORT, TASK_COMMAND_CREATE, false, "create task"));
		options.addOption(new Option(TASK_COMMAND_LIST_ASSIGNED_SHORT, TASK_COMMAND_LIST_ASSIGNED, false,
				"task list assigned on user"));
		options.addOption(new Option(TASK_COMMAND_LIST_CREATED_SHORT, TASK_COMMAND_LIST_CREATED, false,
				"task list created by user"));
		options.addOption(new Option(TASK_COMMAND_DELETE_SHORT, TASK_COMMAND_DELETE, false, "delete task"));
		options.addOption(new Option(TASK_COMMAND_COMPLETE_SHORT, TASK_COMMAND_COMPLETE, false, "complete task"));
	}

	public boolean isTaskCommand(CommandLine commandLine) {
		return isCommand(commandLine);
	}

}
