package ru.korotkov.express.taskmanager.client.executor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.korotkov.express.taskmanager.client.command.CommandParam;
import ru.korotkov.express.taskmanager.client.command.CommandParser;
import ru.korotkov.express.taskmanager.client.executor.cli.CliCommandExecutor;
import ru.korotkov.express.taskmanager.client.executor.cli.CliTaskCommandExecutor;
import ru.korotkov.express.taskmanager.client.executor.cli.CliUserSessionExecutor;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponse;
import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponseCode;

public class CommandExecutorImpl implements CommandExecutor {

	private final List<CliCommandExecutor> executors = new ArrayList<>();

	private final CommandParam commandParam = new CommandParam();

	private final Logger logger = LogManager.getLogger(this);

	private final Options allCommands = new Options();

	

	public CommandExecutorImpl() {
		executors.add(new CliTaskCommandExecutor());
		executors.add(new CliUserSessionExecutor());
		executors.forEach(executor -> executor.getOptionsCollection().forEach(allCommands::addOption));
		commandParam.getOptions().getOptions().forEach(allCommands::addOption);
	}

	@Override
	public CommandExecutorResponse executeCommand(String command, String login) {
		try {
			var commandLine = CommandParser.parse(command, allCommands);
			return delegateExectuionToSpecialListener(commandLine, login);
		} catch (ParseException e) {
			logger.error("Unkown command: {}", command, e);
			return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
		}
	}

	private CommandExecutorResponse delegateExectuionToSpecialListener(CommandLine commandLine, String login) {
		for (CliCommandExecutor executor : executors) {
			if (executor.isCommandForThisExecutor(commandLine))
				return executor.executeCommand(commandLine, login);
		}
		return new CommandExecutorResponse(CommandExecutorResponseCode.COMMAND_COMPLETED_WITH_ERROR);
	}

}
