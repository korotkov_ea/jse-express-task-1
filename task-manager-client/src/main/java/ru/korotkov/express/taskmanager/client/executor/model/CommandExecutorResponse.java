package ru.korotkov.express.taskmanager.client.executor.model;

/**
 * Response of
 * {@link ru.korotkov.express.taskmanager.client.executor.CommandExecutor}
 * 
 * @author RemanentCrane
 */
public class CommandExecutorResponse {

	private CommandExecutorResponseCode code;

	private String codeDescription;

	public CommandExecutorResponse(CommandExecutorResponseCode code) {
		this.code = code;
		setCodeDescriptionFromCode();
	}

	public CommandExecutorResponseCode getCode() {
		return code;
	}

	public void setCode(CommandExecutorResponseCode code) {
		this.code = code;
	}

	public CommandExecutorResponse code(CommandExecutorResponseCode code) {
		this.code = code;
		return this;
	}

	public String getCodeDescription() {
		return codeDescription;
	}

	private void setCodeDescriptionFromCode() {
		switch (this.code) {
			case COMMAND_SUCCESSFULLY_COMPLETED:
				this.codeDescription = "Command was successfully completed";
				break;
			case COMMAND_COMPLETED_WITH_ERROR:
				this.codeDescription = "Error while executing command";
				break;
			case COMMAND_EXIT:
				this.codeDescription = "Exit from task-manager";
				break;
			default:
				break;
		}
	}

	@Override
	public String toString() {

		return "Command executor response: {\n\t\"code\": " + code + ",\n\t\"codeDescription\": " + codeDescription
				+ "\n}";
	}

}
