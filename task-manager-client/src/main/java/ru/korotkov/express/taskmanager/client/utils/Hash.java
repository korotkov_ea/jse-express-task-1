package ru.korotkov.express.taskmanager.client.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class Hash {

	private Hash() {

	}

	public static String hashPassword(String password) {
		return DigestUtils.sha512Hex(password);
	}

}