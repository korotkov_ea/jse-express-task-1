package ru.korotkov.express.taskmanager.client.command;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;

public class CommandParam extends Command {

	public static final String COMMAND_PARAM_NAME = "name";
	public static final String COMMAND_PARAM_NAME_SHORT = "n";

	public static final String COMMAND_PARAM_TASK_ID = "task_id";
	public static final String COMMAND_PARAM_TASK_ID_SHORT = "tid";

	public static final String COMMAND_PARAM_TASK_ASSIGNED = "task_assigned_login";
	public static final String COMMAND_PARAM_TASK_ASSIGNED_SHORT = "tal";

	public static final String COMMAND_PARAM_TASK_CREATED = "task_created_login";
	public static final String COMMAND_PARAM_TASK_CREATED_SHORT = "tcl";

	public static final String COMMAND_PARAM_USER_LOGIN = "user_login";
	public static final String COMMAND_PARAM_USER_LOGIN_SHORT = "l";

	public static final String COMMAND_PARAM_USER_PASSWORD = "password";
	public static final String COMMAND_PARAM_USER_PASSWORD_SHORT = "p";

	public CommandParam() {
		options.addOption(new Option(COMMAND_PARAM_NAME_SHORT, COMMAND_PARAM_NAME, true, "name of task/project param"));
		options.addOption(new Option(COMMAND_PARAM_TASK_ID_SHORT, COMMAND_PARAM_TASK_ID, true, "task id param"));
		options.addOption(new Option(COMMAND_PARAM_USER_LOGIN_SHORT, COMMAND_PARAM_USER_LOGIN, true, "user login"));
		options.addOption(
				new Option(COMMAND_PARAM_USER_PASSWORD_SHORT, COMMAND_PARAM_USER_PASSWORD, true, "user password"));
		options.addOption(new Option(COMMAND_PARAM_USER_LOGIN_SHORT, COMMAND_PARAM_USER_LOGIN, true, "user login"));
		options.addOption(new Option(COMMAND_PARAM_TASK_ASSIGNED_SHORT, COMMAND_PARAM_TASK_ASSIGNED, true,
				"user login who assigned on this task"));
		options.addOption(new Option(COMMAND_PARAM_TASK_CREATED_SHORT, COMMAND_PARAM_TASK_CREATED, true,
				"user login who created on this task"));

	}

	public boolean isCommandParam(CommandLine commandLine) {
		return isCommand(commandLine);
	}

}
