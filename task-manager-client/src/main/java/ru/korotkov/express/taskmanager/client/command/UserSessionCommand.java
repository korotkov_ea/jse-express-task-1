package ru.korotkov.express.taskmanager.client.command;

import org.apache.commons.cli.CommandLine;

public class UserSessionCommand extends Command {

	public static final String USER_SESSION_COMMAND_LOGIN = "login";
	public static final String USER_SESSION_COMMAND_LOGOUT = "logout";

	public UserSessionCommand() {
		options.addOption(USER_SESSION_COMMAND_LOGIN, false, "login to user session");
		options.addOption(USER_SESSION_COMMAND_LOGOUT, false, "logout from user session and exit from task manager");
	}

	public boolean isUserSessionCommand(CommandLine commandLine) {
		return isCommand(commandLine);
	}

}
