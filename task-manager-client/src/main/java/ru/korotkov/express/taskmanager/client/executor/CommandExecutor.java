package ru.korotkov.express.taskmanager.client.executor;

import ru.korotkov.express.taskmanager.client.executor.model.CommandExecutorResponse;

public interface CommandExecutor {

	/**
	 * Execute incoming command
	 * 
	 * @param command - command
	 * @param login  - user identifier
	 * @return response code {@link CommandExecutorResponse}
	 */
	CommandExecutorResponse executeCommand(final String command, final String login);

}
