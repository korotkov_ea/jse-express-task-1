package ru.korotkov.express.taskmanager.client.command;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;


public class CommandParser {

	private CommandParser() {

	}

	public static CommandLine parse(String command, Options options) throws ParseException {
		List<String> commandsList = new ArrayList<>();
		Matcher matcher = Pattern.compile("([^\"]\\S*|\"[^>]+\")\\s*").matcher(command);
		while (matcher.find()) {
			commandsList.add(matcher.group(1));
		}
		String[] commands = new String[commandsList.size()];
		commandsList.toArray(commands);
		CommandLineParser parser = new DefaultParser();
		try {
			return parser.parse(options, commands);
		} catch (ParseException exception) {
			throw new ParseException("Unknown command: " + command);
		}
	}

}
