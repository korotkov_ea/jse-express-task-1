package ru.korotkov.express.taskmanager.server.rest.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Задача
 */
@ApiModel(description = "Задача")
@Validated
public class Task {

	@JsonProperty("id")
	private Long id = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("assigned")
	private String assigned = null;

	@JsonProperty("created")
	private String created = null;

	@JsonProperty("finished")
	private Boolean finished = null;

	public Task id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Идентификатор задачи
	 * 
	 * @return id
	 **/
	@ApiModelProperty(value = "Идентификатор задачи")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Task name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Название задачи
	 * 
	 * @return name
	 **/
	@ApiModelProperty(value = "Название задачи")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Task assigned(String assigned) {
		this.assigned = assigned;
		return this;
	}

	/**
	 * Идентификатор пользователя, на которого назначена задача
	 * 
	 * @return assigned
	 **/
	@ApiModelProperty(value = "Идентификатор пользователя, на которого назначена задача")
	public String getAssigned() {
		return assigned;
	}

	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}

	public Task created(String created) {
		this.created = created;
		return this;
	}

	/**
	 * Идентификатор пользователя, создавшего задачу
	 * 
	 * @return created
	 **/
	@ApiModelProperty(value = "Идентификатор пользователя, создавшего задачу")
	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Task finished(Boolean finished) {
		this.finished = finished;
		return this;
	}

	/**
	 * Признак выполнения команды (true-выполнена, false-не выполнена)
	 * 
	 * @return finished
	 **/
	@ApiModelProperty(value = "Признак выполнения команды (true-выполнена, false-не выполнена)")
	public Boolean isFinished() {
		return finished;
	}

	public void setFinished(Boolean finished) {
		this.finished = finished;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Task task = (Task) o;
		return Objects.equals(this.id, task.id) && Objects.equals(this.name, task.name)
				&& Objects.equals(this.assigned, task.assigned) && Objects.equals(this.created, task.created)
				&& Objects.equals(this.finished, task.finished);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, assigned, created, finished);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Task {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    assigned: ").append(toIndentedString(assigned)).append("\n");
		sb.append("    created: ").append(toIndentedString(created)).append("\n");
		sb.append("    finished: ").append(toIndentedString(finished)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
