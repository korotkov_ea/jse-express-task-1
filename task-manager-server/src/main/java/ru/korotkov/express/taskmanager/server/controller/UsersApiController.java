package ru.korotkov.express.taskmanager.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import ru.korotkov.express.taskmanager.core.exception.user.UserInvalidParamException;
import ru.korotkov.express.taskmanager.core.exception.user.UserNotFoundException;
import ru.korotkov.express.taskmanager.core.service.UserService;
import ru.korotkov.express.taskmanager.server.rest.api.UsersApi;
import ru.korotkov.express.taskmanager.server.rest.model.User;
import ru.korotkov.express.taskmanager.server.utils.EntityToModelConverter;

@RestController
public class UsersApiController implements UsersApi {

	private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

	private UserService userService;

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public ResponseEntity<User> getUser(
			@ApiParam(value = "Логин пользователя", required = true) @PathVariable("login") String login) {
		try {
			var user = userService.findUserByLogin(login);
			return new ResponseEntity<>(EntityToModelConverter.convertUserEntityToUser(user), HttpStatus.OK);
		} catch (UserInvalidParamException | UserNotFoundException e) {
			log.error("Error while getting user: ", e);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

}
