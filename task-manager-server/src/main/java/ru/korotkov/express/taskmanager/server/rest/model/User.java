package ru.korotkov.express.taskmanager.server.rest.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Пользователь
 */
@ApiModel(description = "Пользователь")
@Validated
public class User {

	@JsonProperty("login")
	private String login = null;

	@JsonProperty("password")
	private String password = null;

	public User login(String login) {
		this.login = login;
		return this;
	}

	/**
	 * Имя пользователя
	 * 
	 * @return login
	 **/
	@ApiModelProperty(value = "Имя пользователя")

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public User password(String password) {
		this.password = password;
		return this;
	}

	/**
	 * Пароль
	 * 
	 * @return password
	 **/
	@ApiModelProperty(value = "Пароль")

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		User user = (User) o;
		return Objects.equals(this.login, user.login) && Objects.equals(this.password, user.password);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login, password);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class User {\n");
		sb.append("    login: ").append(toIndentedString(login)).append("\n");
		sb.append("    password: ").append(toIndentedString(password)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
