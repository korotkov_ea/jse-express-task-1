package ru.korotkov.express.taskmanager.server.rest.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModelProperty;

/**
 * Body1
 */
@Validated

public class TaskBody {

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("assigned")
	private String assigned = null;

	@JsonProperty("created")
	private String created = null;

	public TaskBody name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Название задачи
	 * 
	 * @return name
	 **/
	@ApiModelProperty(value = "Название задачи")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TaskBody assigned(String assigned) {
		this.assigned = assigned;
		return this;
	}

	/**
	 * Идентификатор пользователя, на которого назначена задача
	 * 
	 * @return assigned
	 **/
	@ApiModelProperty(value = "Идентификатор пользователя, на которого назначена задача")
	public String getAssigned() {
		return assigned;
	}

	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}

	public TaskBody created(String created) {
		this.created = created;
		return this;
	}

	/**
	 * Идентификатор пользователя, создавшего задачу
	 * 
	 * @return created
	 **/
	@ApiModelProperty(value = "Идентификатор пользователя, создавшего задачу")
	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TaskBody body1 = (TaskBody) o;
		return Objects.equals(this.name, body1.name) && Objects.equals(this.assigned, body1.assigned)
				&& Objects.equals(this.created, body1.created);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, assigned, created);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Body1 {\n");

		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    assigned: ").append(toIndentedString(assigned)).append("\n");
		sb.append("    created: ").append(toIndentedString(created)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
