package ru.korotkov.express.taskmanager.server.utils;

import ru.korotkov.express.taskmanager.core.base.entity.TaskEntity;
import ru.korotkov.express.taskmanager.core.base.entity.UserEntity;
import ru.korotkov.express.taskmanager.server.rest.model.Task;
import ru.korotkov.express.taskmanager.server.rest.model.User;

public class EntityToModelConverter {

	private EntityToModelConverter() {

	}

	public static Task convertTaskEntityToTask(TaskEntity taskEntity) {
		return new Task().id(taskEntity.getId()).assigned(taskEntity.getAssigned()).created(taskEntity.getCreated())
				.finished(taskEntity.isFinished());
	}

	public static User convertUserEntityToUser(UserEntity userEntity) {
		return new User().login(userEntity.getLogin()).password(userEntity.getPassword());
	}

}
