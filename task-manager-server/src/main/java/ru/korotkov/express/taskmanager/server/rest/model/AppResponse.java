package ru.korotkov.express.taskmanager.server.rest.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Ответ приложения
 */
@ApiModel(description = "Ответ приложения")
@Validated
public class AppResponse {

	@JsonProperty("code")
	private Integer code = null;

	@JsonProperty("message")
	private String message = null;

	public AppResponse code(Integer code) {
		this.code = code;
		return this;
	}

	/**
	 * Код ответа
	 * 
	 * @return code
	 **/
	@ApiModelProperty(value = "Код ответа")

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public AppResponse message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Текст ответа
	 * 
	 * @return message
	 **/
	@ApiModelProperty(value = "Текст ответа")

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AppResponse appResponse = (AppResponse) o;
		return Objects.equals(this.code, appResponse.code) && Objects.equals(this.message, appResponse.message);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, message);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class AppResponse {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
