package ru.korotkov.express.taskmanager.server.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ru.korotkov.express.taskmanager.server.rest.model.User;

@Validated
@Api(value = "users")
public interface UsersApi {

	@ApiOperation(value = "Найти пользователя", nickname = "getUser", notes = "", response = User.class, tags = {
			"users", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Пользователь с заданным логином", response = User.class) })
	@GetMapping(value = "/users/{login}", produces = { "application/json" })
	ResponseEntity<User> getUser(
			@ApiParam(value = "Логин пользователя", required = true) @PathVariable("login") String login);

}
