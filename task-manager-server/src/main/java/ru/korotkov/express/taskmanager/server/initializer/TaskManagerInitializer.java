package ru.korotkov.express.taskmanager.server.initializer;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import ru.korotkov.express.taskmanager.core.base.config.DataSourceConfig;
import ru.korotkov.express.taskmanager.core.base.config.JpaConfig;
import ru.korotkov.express.taskmanager.core.base.config.LiquibaseConfig;
import ru.korotkov.express.taskmanager.server.config.AppConfig;
import ru.korotkov.express.taskmanager.server.config.WebConfig;

public class TaskManagerInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { DataSourceConfig.class, LiquibaseConfig.class, JpaConfig.class, AppConfig.class };
	}

	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { WebConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
