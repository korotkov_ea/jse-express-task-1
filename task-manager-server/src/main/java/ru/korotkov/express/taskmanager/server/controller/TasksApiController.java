package ru.korotkov.express.taskmanager.server.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import ru.korotkov.express.taskmanager.core.base.entity.TaskEntity;
import ru.korotkov.express.taskmanager.core.exception.task.TaskInvalidParamException;
import ru.korotkov.express.taskmanager.core.exception.task.TaskNotFoundException;
import ru.korotkov.express.taskmanager.core.service.TaskService;
import ru.korotkov.express.taskmanager.server.rest.api.TasksApi;
import ru.korotkov.express.taskmanager.server.rest.model.AppResponse;
import ru.korotkov.express.taskmanager.server.rest.model.Task;
import ru.korotkov.express.taskmanager.server.rest.model.TaskBody;
import ru.korotkov.express.taskmanager.server.utils.EntityToModelConverter;

@RestController
public class TasksApiController implements TasksApi {

	private static final Logger log = LoggerFactory.getLogger(TasksApiController.class);

	private TaskService taskService;

	@Autowired
	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public ResponseEntity<AppResponse> completeTask(
			@ApiParam(value = "Идентификатор задачи", required = true) @PathVariable("taskId") Long taskId,
			@NotNull @ApiParam(value = "Логин пользователя", required = true) @Valid @RequestParam(value = "login", required = true) String login) {
		try {
			var task = taskService.findTaskById(taskId);
			if (!Objects.equals(task.getAssigned(), login))
				return new ResponseEntity<>(
						new AppResponse().code(1).message("Access denied. Task is not assigned on you."),
						HttpStatus.OK);
			taskService.completeTask(taskId);
			return new ResponseEntity<>(new AppResponse().code(0).message("Task completed."), HttpStatus.OK);
		} catch (TaskNotFoundException | TaskInvalidParamException e) {
			log.error("Error while completing task:", e);
			return new ResponseEntity<>(
					new AppResponse().code(1).message("Error while completing task:" + e.getMessage()), HttpStatus.OK);
		}
	}

	public ResponseEntity<AppResponse> createTask(@Valid @RequestBody TaskBody body) {
		try {
			var task = new TaskEntity().id(System.nanoTime()).assigned(body.getAssigned()).created(body.getCreated())
					.name(body.getName()).finished(false);
			task = taskService.createTask(task);
			return new ResponseEntity<>(new AppResponse().code(0).message("Created task: " + task), HttpStatus.OK);
		} catch (TaskInvalidParamException e) {
			log.error("Error while creating task: ", e);
			return new ResponseEntity<>(
					new AppResponse().code(1).message("Error while creating task: " + e.getMessage()), HttpStatus.OK);
		}
	}

	public ResponseEntity<List<Task>> getAssignedTaskList(
			@ApiParam(value = "Логин пользователя", required = true) @PathVariable("login") String login) {
		try {
			var tasks = taskService.findAssignedTasksOnUser(login);
			List<Task> response = tasks.stream().map(EntityToModelConverter::convertTaskEntityToTask)
					.collect(Collectors.toList());
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (TaskInvalidParamException e) {
			log.error("Error while getting assigned task list: ", e);
			return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
		}
	}

	public ResponseEntity<List<Task>> getCreatedTaskList(
			@ApiParam(value = "Логин пользователя", required = true) @PathVariable("login") String login) {
		try {
			var tasks = taskService.findCreatedTasksByUser(login);
			List<Task> response = tasks.stream().map(EntityToModelConverter::convertTaskEntityToTask)
					.collect(Collectors.toList());
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (TaskInvalidParamException e) {
			log.error("Error while getting created task list: ", e);
			return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
		}
	}

	public ResponseEntity<List<Task>> getTasksList() {
		// P.S. функционал, который не должен будет пользоваться клиентом. Данная
		// функция была добавлена просто, чтоб была
		return new ResponseEntity<>(taskService.findAllTasks().stream()
				.map(EntityToModelConverter::convertTaskEntityToTask).collect(Collectors.toList()), HttpStatus.OK);

	}

	public ResponseEntity<AppResponse> removeTask(
			@ApiParam(value = "Идентификатор задачи", required = true) @PathVariable("taskId") Long taskId,
			@NotNull @ApiParam(value = "Логин пользователя", required = true) @Valid @RequestParam(value = "login", required = true) String login) {
		try {
			var task = taskService.findTaskById(taskId);
			if (!Objects.equals(task.getAssigned(), login) || !Objects.equals(task.getCreated(), login))
				return new ResponseEntity<>(
						new AppResponse().code(1).message("Access denied. You can't remove that task."), HttpStatus.OK);
			taskService.deleteTask(taskId);
			return new ResponseEntity<>(new AppResponse().code(0).message("Task removed."), HttpStatus.OK);
		} catch (TaskInvalidParamException | TaskNotFoundException e) {
			log.error("Error while removing task: ", e);
			return new ResponseEntity<>(
					new AppResponse().code(1).message("Error while removing task: " + e.getMessage()), HttpStatus.OK);
		}
	}

	
	public ResponseEntity<Task> getTask(
			@ApiParam(value = "Идентификатор задачи", required = true) @PathVariable("taskId") Long taskId) {
		try {
			var task = taskService.findTaskById(taskId);
			return new ResponseEntity<>(EntityToModelConverter.convertTaskEntityToTask(task), HttpStatus.OK);
		} catch (TaskNotFoundException | TaskInvalidParamException e) {
			log.error("Error while getting task: ", e);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
