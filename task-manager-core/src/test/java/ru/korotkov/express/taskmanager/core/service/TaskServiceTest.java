package ru.korotkov.express.taskmanager.core.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ru.korotkov.express.taskmanager.core.base.entity.TaskEntity;
import ru.korotkov.express.taskmanager.core.exception.task.TaskInvalidParamException;
import ru.korotkov.express.taskmanager.core.exception.task.TaskNotFoundException;
import ru.korotkov.express.taskmanager.core.repository.TaskRepository;

public class TaskServiceTest {

	private static final TaskRepository taskRepository = mock(TaskRepository.class);
	private static final TaskService taskService = new TaskService();

	@BeforeAll
	static void setUp() {
		taskService.setTaskRepository(taskRepository);
	}

	@Test
	void testCreateTaskCorrect() throws TaskInvalidParamException {
		var task = new TaskEntity().id(System.nanoTime()).name("name").assigned("assigned").created("created");
		when(taskRepository.createTask(task)).thenReturn(task);
		var createdTask = taskService.createTask(task);
		assertEquals(task, createdTask);
	}

	@Test
	void testCreateTaskWhenTaskIsNull() {
		assertThrows(TaskInvalidParamException.class, () -> taskService.createTask(null));
	}

	@Test
	void testCreateTaskWhenTaskIdIsNull() {
		var task = new TaskEntity().name("name").assigned("assigned").created("created");
		assertThrows(TaskInvalidParamException.class, () -> taskService.createTask(task));
	}

	@Test
	void testCreateTaskWhenNameIsNull() {
		var task = new TaskEntity().id(System.nanoTime()).assigned("assigned").created("created");
		assertThrows(TaskInvalidParamException.class, () -> taskService.createTask(task));
	}

	@Test
	void testCreateTaskWhenAssignedIsNull() {
		var task = new TaskEntity().id(System.nanoTime()).name("name").created("created");
		assertThrows(TaskInvalidParamException.class, () -> taskService.createTask(task));
	}

	@Test
	void testCreateTaskWhenCreatedIsNull() {
		var task = new TaskEntity().id(System.nanoTime()).name("name").assigned("assigned");
		assertThrows(TaskInvalidParamException.class, () -> taskService.createTask(task));
	}

	@Test
	void testFindByIdCorrect() throws TaskNotFoundException, TaskInvalidParamException {
		var id = 0L;
		var expectedTask = new TaskEntity().id(id);
		when(taskRepository.findTaskById(id)).thenReturn(new TaskEntity().id(id));
		assertEquals(expectedTask, taskService.findTaskById(id));
	}

	@Test
	void testFindByIdWhenIdIsNull() {
		assertThrows(TaskInvalidParamException.class, () -> taskService.findTaskById(null));
	}

	@Test
	void testFindByIdWithTaskNotFoundException() {
		var id = 0L;
		when(taskRepository.findTaskById(id)).thenReturn(null);
		assertThrows(TaskNotFoundException.class, () -> taskService.findTaskById(id));
	}

	@Test
	void testFindAllTasks() {
		List<TaskEntity> tasks = new ArrayList<>();
		var expectedCount = 0;
		when(taskRepository.findAllTasks()).thenReturn(tasks);
		assertEquals(expectedCount, taskService.findAllTasks().size());
		for (long i = 0L; i < 10; i++) {
			expectedCount++;
			var task = new TaskEntity().id(i);
			tasks.add(task);
			assertEquals(expectedCount, taskService.findAllTasks().size());
		}
	}

	@Test
	void testFindAssignedTaskOnUserCorrect() throws TaskInvalidParamException {
		List<TaskEntity> tasks = new ArrayList<>();
		var expectedCount = 0;
		when(taskRepository.findAssignedTasksOnUser("login")).thenReturn(tasks);
		assertEquals(expectedCount, taskService.findAssignedTasksOnUser("login").size());
		for (long i = 0L; i < 10; i++) {
			expectedCount++;
			var task = new TaskEntity().id(i).assigned("login");
			tasks.add(task);
			assertEquals(expectedCount, taskService.findAssignedTasksOnUser("login").size());
		}
	}

	@Test
	void testFindAssignedTaskOnUserWhenLoginIsNull() {
		assertThrows(TaskInvalidParamException.class, () -> taskService.findAssignedTasksOnUser(null));
		assertThrows(TaskInvalidParamException.class, () -> taskService.findAssignedTasksOnUser(""));
	}

	@Test
	void testFindCreatedTasksByUserCorrect() throws TaskInvalidParamException {
		List<TaskEntity> tasks = new ArrayList<>();
		var expectedCount = 0;
		when(taskRepository.findCreatedTasksByUser("login")).thenReturn(tasks);
		assertEquals(expectedCount, taskService.findCreatedTasksByUser("login").size());
		for (long i = 0L; i < 10; i++) {
			expectedCount++;
			var task = new TaskEntity().id(i).assigned("login");
			tasks.add(task);
			assertEquals(expectedCount, taskService.findCreatedTasksByUser("login").size());
		}
	}

	@Test
	void testFindCreatedTasksByUserWhenLoginIsNull() {
		assertThrows(TaskInvalidParamException.class, () -> taskService.findCreatedTasksByUser(null));
		assertThrows(TaskInvalidParamException.class, () -> taskService.findCreatedTasksByUser(""));
	}

	@Test
	void testCompleteTaskCorrect() throws TaskInvalidParamException, TaskNotFoundException {
		var id = 0L;
		var task = new TaskEntity().id(id).finished(false);
		when(taskRepository.findTaskById(id)).thenReturn(task);
		when(taskRepository.completeTask(task)).thenReturn(task);
		assertTrue(taskService.completeTask(id).isFinished());
	}
	
	@Test
	void testCompleteTaskWhenIdIsNull() {
		assertThrows(TaskInvalidParamException.class, ()-> taskService.completeTask(null));
	}

	@Test
	void testCompleteTaskWhenTaskNotFound() {
		var id=0L;
		when(taskRepository.findTaskById(id)).thenReturn(null);
		assertThrows(TaskNotFoundException.class, ()-> taskService.completeTask(id));
	}

	@Test
	void testDeleteTaskCorrect() throws TaskInvalidParamException, TaskNotFoundException {
		var id = 0L;
		when(taskRepository.findTaskById(id)).thenReturn(new TaskEntity());
		doNothing().when(taskRepository).deleteTask(id);
		assertDoesNotThrow(() -> taskService.deleteTask(id));
	}

	@Test
	void testDeleteTaskWhenIdIsNull() {
		assertThrows(TaskInvalidParamException.class, () -> taskService.deleteTask(null));
	}

	@Test
	void testDeleteTaskWhenTaskNotFound() {
		var id = 0L;
		when(taskRepository.findTaskById(id)).thenReturn(null);
		assertThrows(TaskNotFoundException.class, () -> taskService.deleteTask(id));
	}

}
