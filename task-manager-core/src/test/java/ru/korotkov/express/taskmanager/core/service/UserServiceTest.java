package ru.korotkov.express.taskmanager.core.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ru.korotkov.express.taskmanager.core.base.entity.UserEntity;
import ru.korotkov.express.taskmanager.core.exception.user.UserInvalidParamException;
import ru.korotkov.express.taskmanager.core.exception.user.UserNotFoundException;
import ru.korotkov.express.taskmanager.core.repository.UserRepository;

public class UserServiceTest {
	
	private static final UserRepository userRepository = mock(UserRepository.class);
	private static final UserService userService = new UserService();

	@BeforeAll
	static void setUp() {
		userService.setUserRepository(userRepository);
	}

	@Test
	void testFindUserByLoginCorrect() throws UserInvalidParamException, UserNotFoundException {
		var login = "login";
		var expectedUser = new UserEntity().login(login);
		when(userRepository.findUserByLogin(login)).thenReturn(new UserEntity().login(login));
		assertEquals(expectedUser, userService.findUserByLogin(login));
	}

	@Test
	void testFindUserByLoginWhenLoginIsNullOrEmpty() {
		assertThrows(UserInvalidParamException.class, ()-> userService.findUserByLogin(null));
		assertThrows(UserInvalidParamException.class, ()-> userService.findUserByLogin(""));
	}
	
	@Test
	void testFindUserByLoginWhenFindedUserIsNull() {
		var login = "login";
		when(userRepository.findUserByLogin(login)).thenReturn(null);
		assertThrows(UserNotFoundException.class, ()-> userService.findUserByLogin("login"));
	}

}
