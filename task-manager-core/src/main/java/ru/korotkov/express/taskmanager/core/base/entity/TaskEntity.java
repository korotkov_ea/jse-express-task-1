package ru.korotkov.express.taskmanager.core.base.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tasks")
public class TaskEntity {

	@Id
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "assigned", nullable = false)
	private String assigned;

	@Column(name = "created", nullable = false)
	private String created;

	@Column(name = "finished", nullable = false)
	private boolean finished;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TaskEntity id(Long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TaskEntity name(String name) {
		this.name = name;
		return this;
	}

	public String getAssigned() {
		return assigned;
	}

	public void setAssigned(String assigned) {
		this.assigned = assigned;
	}

	public TaskEntity assigned(String assigned) {
		this.assigned = assigned;
		return this;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public TaskEntity created(String created) {
		this.created = created;
		return this;

	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public TaskEntity finished(boolean finished) {
		this.finished = finished;
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getName(), getCreated(), getAssigned(), isFinished());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof TaskEntity))
			return false;
		var task = (TaskEntity) obj;
		if (!Objects.equals(getId(), task.getId()))
			return false;
		if (!Objects.equals(getName(), task.getName()))
			return false;
		if (!Objects.equals(getCreated(), task.getCreated()))
			return false;
		if (!Objects.equals(getAssigned(), task.getAssigned()))
			return false;
		return (Objects.equals(isFinished(), task.isFinished()));
	}

	@Override
	public String toString() {
		var classDescription = "\nTask: {";
		classDescription += "\n\tid: " + id;
		classDescription += ",\n\tname: " + name;
		classDescription += ",\n\tcreated: " + created;
		classDescription += ",\n\tassigned: " + assigned;
		classDescription += ",\n\tfinished: " + finished;
		classDescription += "}\n--------------------";
		return classDescription;
	}

}
