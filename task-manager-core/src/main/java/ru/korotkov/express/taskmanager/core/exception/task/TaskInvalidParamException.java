package ru.korotkov.express.taskmanager.core.exception.task;

import java.text.MessageFormat;

import ru.korotkov.express.taskmanager.core.enumarated.InvalidParamName;

public class TaskInvalidParamException extends TaskException {

	/** Serial version UID */
	private static final long serialVersionUID = 2721693917261854004L;

	public TaskInvalidParamException(final InvalidParamName invalidParamName, final Object invalidParam) {
		super(MessageFormat.format("Error while manipulating with task. Entered param {0} is not correct: {1}",
				invalidParamName, invalidParam));
	}

}
