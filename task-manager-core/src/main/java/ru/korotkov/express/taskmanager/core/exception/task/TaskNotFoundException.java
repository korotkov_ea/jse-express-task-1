package ru.korotkov.express.taskmanager.core.exception.task;

import java.text.MessageFormat;

public class TaskNotFoundException extends TaskException {

	/** Serial version UID */
	private static final long serialVersionUID = 709954456069675083L;

	public TaskNotFoundException(final Long id) {
		super(MessageFormat.format("Task with id \"{0}\" not found", id));
	}

}

