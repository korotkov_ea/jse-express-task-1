package ru.korotkov.express.taskmanager.core.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.korotkov.express.taskmanager.core.base.entity.UserEntity;

@Repository
@Transactional
public class UserRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public UserEntity findUserByLogin(String login) {
		return entityManager.find(UserEntity.class, login);
	}

}
