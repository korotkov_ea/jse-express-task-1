package ru.korotkov.express.taskmanager.core.exception.user;

import java.text.MessageFormat;

import ru.korotkov.express.taskmanager.core.enumarated.InvalidParamName;

public class UserInvalidParamException extends UserException {

	/** serial version UID */
	private static final long serialVersionUID = 3328031457940590285L;

	
	public UserInvalidParamException(final InvalidParamName invalidParamName, final Object invalidParam) {
		super(MessageFormat.format("Error while manipulating with user. Entered param {0} is not correct: {1}",
				invalidParamName, invalidParam));
	}

}
