package ru.korotkov.express.taskmanager.core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.korotkov.express.taskmanager.core.base.entity.TaskEntity;
import ru.korotkov.express.taskmanager.core.enumarated.InvalidParamName;
import ru.korotkov.express.taskmanager.core.exception.task.TaskInvalidParamException;
import ru.korotkov.express.taskmanager.core.exception.task.TaskNotFoundException;
import ru.korotkov.express.taskmanager.core.repository.TaskRepository;

@Service
public class TaskService {

	private TaskRepository taskRepository;

	@Autowired
	public void setTaskRepository(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}

	@Transactional
	public TaskEntity createTask(TaskEntity task) throws TaskInvalidParamException {
		if (task == null)
			throw new TaskInvalidParamException(InvalidParamName.TASK, task);
		if (task.getId() == null)
			throw new TaskInvalidParamException(InvalidParamName.TASK_ID, task.getId());
		if (task.getName() == null || task.getName().isEmpty())
			throw new TaskInvalidParamException(InvalidParamName.TASK_NAME, task.getName());
		if (task.getAssigned() == null || task.getAssigned().isEmpty())
			throw new TaskInvalidParamException(InvalidParamName.TASK_ASSIGNED, task.getAssigned());
		if (task.getCreated() == null || task.getCreated().isEmpty())
			throw new TaskInvalidParamException(InvalidParamName.TASK_CREATED, task.getCreated());
		return taskRepository.createTask(task);
	}
	@Transactional
	public TaskEntity findTaskById(Long id) throws TaskNotFoundException, TaskInvalidParamException {
		if (id == null)
			throw new TaskInvalidParamException(InvalidParamName.TASK_ID, id);
		var task = taskRepository.findTaskById(id);
		if (task == null)
			throw new TaskNotFoundException(id);
		return task;
	}
	@Transactional
	public List<TaskEntity> findAllTasks() {
		return taskRepository.findAllTasks();
	}
	@Transactional
	public List<TaskEntity> findAssignedTasksOnUser(String login) throws TaskInvalidParamException {
		if (login == null || login.isEmpty())
			throw new TaskInvalidParamException(InvalidParamName.USER_LOGIN, login);
		return taskRepository.findAssignedTasksOnUser(login);
	}
	@Transactional
	public List<TaskEntity> findCreatedTasksByUser(String login) throws TaskInvalidParamException {
		if (login == null || login.isEmpty())
			throw new TaskInvalidParamException(InvalidParamName.USER_LOGIN, login);
		return taskRepository.findCreatedTasksByUser(login);
	}
	@Transactional
	public TaskEntity completeTask(final Long id) throws TaskInvalidParamException, TaskNotFoundException {
		if (id == null)
			throw new TaskInvalidParamException(InvalidParamName.TASK_ID, id);
		var task = taskRepository.findTaskById(id);
		if (task == null)
			throw new TaskNotFoundException(id);
		task.setFinished(true);
		return taskRepository.completeTask(task);
	}
	@Transactional
	public void deleteTask(Long id) throws TaskInvalidParamException, TaskNotFoundException {
		if (id == null)
			throw new TaskInvalidParamException(InvalidParamName.TASK_ID, id);
		var task = taskRepository.findTaskById(id);
		if (task == null)
			throw new TaskNotFoundException(id);
		taskRepository.deleteTask(id);
	}

}
