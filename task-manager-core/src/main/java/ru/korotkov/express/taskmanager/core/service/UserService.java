package ru.korotkov.express.taskmanager.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.korotkov.express.taskmanager.core.base.entity.UserEntity;
import ru.korotkov.express.taskmanager.core.enumarated.InvalidParamName;
import ru.korotkov.express.taskmanager.core.exception.user.UserInvalidParamException;
import ru.korotkov.express.taskmanager.core.exception.user.UserNotFoundException;
import ru.korotkov.express.taskmanager.core.repository.UserRepository;

@Service
public class UserService {

	private UserRepository userRepository;

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Transactional
	public UserEntity findUserByLogin(String login) throws UserInvalidParamException, UserNotFoundException {
		if (login == null || login.isEmpty())
			throw new UserInvalidParamException(InvalidParamName.USER_LOGIN, login);
		var user = userRepository.findUserByLogin(login);
		if (user == null)
			throw new UserNotFoundException(login);
		return user;
	}

}
