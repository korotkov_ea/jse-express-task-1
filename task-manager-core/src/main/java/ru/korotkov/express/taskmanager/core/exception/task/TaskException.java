package ru.korotkov.express.taskmanager.core.exception.task;

import java.text.MessageFormat;

public class TaskException extends Exception {

	/** Serial version UID */
	private static final long serialVersionUID = -646169263376113287L;

	public TaskException(String message) {
		super(MessageFormat.format("Error while perfoming manipulation with task : \n{0}", message));
	}

}
