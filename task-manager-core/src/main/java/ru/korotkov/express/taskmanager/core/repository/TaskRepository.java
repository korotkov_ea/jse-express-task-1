package ru.korotkov.express.taskmanager.core.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.korotkov.express.taskmanager.core.base.entity.TaskEntity;

@Repository
public class TaskRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public TaskEntity createTask(TaskEntity task) {
		return entityManager.merge(task);
	}
	
	public TaskEntity findTaskById(Long id) {
		return entityManager.find(TaskEntity.class, id);
	}
	
	public List<TaskEntity> findAllTasks() {
		return entityManager.createQuery("from TaskEntity").getResultList();
	}
	
	public List<TaskEntity> findAssignedTasksOnUser(String login) {
		return entityManager.createQuery("from TaskEntity where assigned = :assigned").setParameter("assigned", login)
		.getResultList();
	}
	
	public List<TaskEntity> findCreatedTasksByUser(String login) {
		return entityManager.createQuery("from TaskEntity where created = :created").setParameter("created", login)
		.getResultList();
	}
	
	@Transactional
	public TaskEntity completeTask(TaskEntity task) {
		return entityManager.merge(task);
	}
	
	@Transactional
	public void deleteTask(Long id) {
		entityManager.remove(entityManager.getReference(TaskEntity.class, id));
	}

}
