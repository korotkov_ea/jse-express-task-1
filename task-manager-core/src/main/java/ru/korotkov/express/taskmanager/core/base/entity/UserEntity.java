package ru.korotkov.express.taskmanager.core.base.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = "login") }, schema = "public")
public class UserEntity {

	@Id
	@Column(name = "login", nullable = false)
	private String login;

	@Column(name = "password", nullable = false)
	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public UserEntity login(String login) {
		this.login = login;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserEntity password(String password) {
		this.password = password;
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLogin(), getPassword());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof UserEntity))
			return false;
		var user = (UserEntity) obj;
		if (!Objects.equals(getLogin(), user.getLogin()))
			return false;
		return (Objects.equals(getPassword(), user.getPassword()));
	}

	@Override
	public String toString() {
		var classDescription = "\nUser: {";
		classDescription += "\n\tlogin: " + login;
		classDescription += ",\n\tpassword: ****";
		classDescription += "}\n--------------------";
		return classDescription;
	}

}
