package ru.korotkov.express.taskmanager.core.exception.user;

import java.text.MessageFormat;

/**
 * Exception class when specified user was not found
 * 
 * @author RemanentCrane
 */
public class UserNotFoundException extends UserException {

	/** serial version UID */
	private static final long serialVersionUID = 3328031457940590283L;

	/**
	 * Constructor for exception
	 * 
	 * @param login - user login
	 */
	public UserNotFoundException(final String login) {
		super(MessageFormat.format("User with login \"{0}\" not found", login));
	}

}
