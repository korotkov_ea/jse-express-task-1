package ru.korotkov.express.taskmanager.core.base.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:jpa.properties")
@ComponentScan("ru.korotkov.express.taskmanager.core")
public class JpaConfig {

	private final Environment environment;

	public JpaConfig(Environment environment) {
		this.environment = environment;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
		var localContainerEntityManager = new LocalContainerEntityManagerFactoryBean();
		localContainerEntityManager.setDataSource(dataSource);
		localContainerEntityManager.setPackagesToScan("ru.korotkov.express.taskmanager.core");

		var vendorAdapter = new HibernateJpaVendorAdapter();
		localContainerEntityManager.setJpaVendorAdapter(vendorAdapter);
		localContainerEntityManager.setJpaProperties(additionalProperties());
		return localContainerEntityManager;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	private Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto"));
		properties.setProperty("hibernate.dialect", environment.getProperty("hibernate.dialect"));
		properties.setProperty("hibernate.show_sql", environment.getProperty("hibernate.show_sql"));
		properties.setProperty("hibernate.format_sql", environment.getProperty("hibernate.format_sql"));
		properties.setProperty("hibernate.connection.url", environment.getProperty("hibernate.connection.url"));
		properties.setProperty("hibernate.connection.driver_class",
				environment.getProperty("hibernate.connection.driver_class"));
		properties.setProperty("hibernate.connection.username",
				environment.getProperty("hibernate.connection.username"));
		properties.setProperty("hibernate.connection.password",
				environment.getProperty("hibernate.connection.password"));
		return properties;
	}
}