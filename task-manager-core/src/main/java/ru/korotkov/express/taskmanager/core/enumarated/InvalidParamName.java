package ru.korotkov.express.taskmanager.core.enumarated;

/**
 * Enum for type invalid param. Using in exceptions like:
 * <ul>
 * <li>{@link ru.korotkov.express.taskmanager.core.exception.task.TaskInvalidParamException}
 * <li>{@link ru.korotkov.express.taskmanager.core.exception.user.UserCreatingException}
 * </ul>
 * 
 * @author RemanentCrane
 */
public enum InvalidParamName {

	TASK("task model"),
	TASK_ID("task identifier"), TASK_NAME("task name"),
	TASK_ASSIGNED("task assigned on user"),
	TASK_CREATED("task created by user"),
	USER_LOGIN("user login"), USER_PASSWORD("user password");

	private final String invalidParam;

	InvalidParamName(final String invalidParam) {
		this.invalidParam = invalidParam;
	}

	public String getInvalidParam() {
		return invalidParam;
	}

	@Override
	public String toString() {
		return "Invalid param: " + invalidParam;
	}

}
