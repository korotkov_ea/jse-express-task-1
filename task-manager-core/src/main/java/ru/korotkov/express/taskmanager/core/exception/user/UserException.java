package ru.korotkov.express.taskmanager.core.exception.user;

import java.text.MessageFormat;

public class UserException extends Exception {

	/** Serial version UID */
	private static final long serialVersionUID = 3128389176666739058L;

	public UserException(final String message) {
		super(MessageFormat.format("Error while perfoming user manipulation: \n{0}", message));
	}

}
